package com.climax.customer_statistic.service;

import com.climax.customer_statistic.repository.CustomerRepository;
import com.climax.customer_statistic.helper.CsvHelper;
import com.climax.customer_statistic.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class CsvServiceImpl implements CsvServiceInterface {


    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public void saveCustomer(MultipartFile file){
        try {
            List<Customer> customers = CsvHelper.csvToCustomers(file.getInputStream());
            customerRepository.saveAll(customers);
        } catch (Exception ex) {
            throw new RuntimeException("fail to store csv data: " + ex.getMessage());
        }
    }

    @Override
    public List<Customer> getAllCustomer() throws Exception {
        try {
            return customerRepository.findAll();
        } catch (Exception ex) {
            throw ex;
        }
    }


}
