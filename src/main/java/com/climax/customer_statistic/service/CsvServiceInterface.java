package com.climax.customer_statistic.service;

import com.climax.customer_statistic.model.Customer;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CsvServiceInterface {

    public void saveCustomer(MultipartFile file);
    public List<Customer> getAllCustomer() throws Exception;


}
