package com.climax.customer_statistic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class CustomerStatisticApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerStatisticApplication.class, args);
	}

}
