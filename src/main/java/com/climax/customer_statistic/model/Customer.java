package com.climax.customer_statistic.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "customer")
@Getter
@Setter
public class Customer{

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "age")
    private Integer age;

    @Column(name = "occupation")
    private String occupation;

    @Column(name = "salary")
    private Integer salary;

    //Constructors
    public Customer() {

    }
    public Customer(String lastName, String firstName, Integer age, String occupation, Integer salary) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.occupation = occupation;
        this.salary = salary;
    }

}
